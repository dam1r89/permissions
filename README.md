TODO: Write tests

## Installation


This package is not published on packagist, so you need to add remote repository manually.

```json
"repositories": [
    {
        "type": "vcs",
        "url": "https://bitbucket.org/dam1r89/permissions.git"
    }
]
```

## Basic usage

Publish migrations

    php artisan vendor:publish --tag=permissions-migrations


Add to `User` model trait.

```php
use SkillFactory\Permissions\HasPermissions;

class User {
    use HasPermissions;
    // ...
}
```

## Usage

This implementation doesn't attach permissions to individual user. Permissions are granted by being part of a group. Group is entity that defines permissions. It can whitelist or blacklist permissions.

Example of group permissions:

```json
{
    "whitelist": [
        "blog",
        "App\\Models\\Property"
    ],
    "blacklist": [
        "blog.secrets",
        "admin",
        "users"
    ]
}
```

Then on place where needed you can query access for given user. For example:

```php
$user->hasPermission('blog');
```

Interesting thing that can be used here are wildcards. This gives more ways of using these permissions.
For example when you have permissions on a certain entity:

Group **moderators**:

```json
{
    "whitelist": [
        "blog.read",
        "blog.create",
        "blog.update"
    ],
    "blacklist": [
        "blog.delete"
    ]
}
```

`$user->hasPermission('blog.*')` will return `false` because it will match `blog.delete` but `$user->hasPermission('blog.read')` will return `true`. This can be used for scoping of access when used correctly.

Group **administrators**:

```json
{
    "whitelist": [
        "blog.read",
        "blog.create",
        "blog.update",
        "blog.delete"
    ],
    "blacklist": []
}
```

`$user->hasPermission('blog.*')` will return `true` because it will match permissions with `blog` prefix.

```php
$user->hasPermission('blog');
```

> This approach can lead to mistake. For example if you create user who is part of both groups - **administrators** and **motrators**. That user will **NOT** have permission to `blog.delete`. Meaning that user should be only part of *administrators* group. 


## Usage on frontend

For frontend usage you can use `$user->permissions` property.
To return this attribute in API responses by default set `protected $appends = ['permissions'];` in `User` model.
Unlike backend usage where you ask user if it has permission, for frontend
you want to define map of predefined permission questions.

For example on backend you would ask

```php
$user->hasPermission('blog');
```

compared to frontend where you would define that property in advance:

```javascript
if (user.permissions.blog === true) {
    // do other stuff   
}
```

To achieve this you need to define every permission you want to assert. To do that
add property in `User` class named `$precalculatedPermissions`.

```php
use SkillFactory\Permissions\HasPermissions;
class User
{
    use HasPermissions;
    
    protected $precalculatedPermissions = [
        'blog' => 'App\\Models\\Blog',
    ];

    protected $appends = ['permissions'];
}
```

## Blade directives

```
@permission('blog.*')

@endpermission
```


