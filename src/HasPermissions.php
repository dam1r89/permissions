<?php

namespace SkillFactory\Permissions;

use SkillFactory\Permissions\Models\Group;

trait HasPermissions
{
    public function groups()
    {
        return $this->belongsToMany(Group::class);
    }

    public function hasPermission($requiredPermission)
    {
        $match = new GlobMatch();
        $whitelisted = false;
        foreach ($this->groups as $group) {
            foreach ($group->getWhitelist() as $permission) {
                $whitelisted = $whitelisted || $match->match($permission, $requiredPermission);
            }
            foreach ($group->getBlacklist() as $permission) {
                if ($match->match($permission, $requiredPermission)) {
                    return false;
                }
            }
        }

        return $whitelisted;
    }

    public function getPermissionsAttribute()
    {
        $allowed = [];
        foreach ($this->precalculatedPermissions as $key => $permission) {
            $allowed[$key] = $this->hasPermission($permission);
        }

        return (object) $allowed;
    }
}
