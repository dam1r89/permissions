<?php

namespace SkillFactory\Permissions;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider
{
    public function register()
    {
    }

    public function boot()
    {
        Blade::directive('permission', function ($permission) {
            return '<?php if (Auth::check() && Auth::user()->hasPermission('.$permission.') || '.$permission.' === null): ?>';
        });

        Blade::directive('endpermission', function ($permission) {
            return '<?php endif; ?>';
        });

        $this->publishes([
            __DIR__.'/migrations/' => database_path('migrations'),
        ], 'permissions-migrations');
    }
}
