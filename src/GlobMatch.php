<?php

namespace SkillFactory\Permissions;

class GlobMatch
{
    public function match($pattern, $string)
    {
        $pattern = $this->escapeAllButKeywords($pattern, $string);

        return 1 === preg_match(sprintf('/^%s$/', $pattern), $string);
    }

    private function escapeAllButKeywords($pattern, $string)
    {
        list($ALL, $CHAR) = $this->getPlaceholders($string);
        $pattern = strtr($pattern, ['*' => $ALL, '?' => $CHAR]);
        $pattern = preg_quote($pattern, '/');
        $pattern = strtr($pattern, [$ALL => '.*', $CHAR => '.']);

        return $pattern;
    }

    private function getPlaceholders($subject)
    {
        $i = 0;
        while (false !== strpos($subject, $placeholder = "__MATCH_PLACEHOLDER_{$i}__")) {
            $i += 1;
        }
        $i += 1;
        $start = $placeholder;
        while (false !== strpos($subject, $placeholder = "__MATCH_PLACEHOLDER_{$i}__")) {
            $i += 1;
        }

        return [$start, $placeholder];
    }
}
