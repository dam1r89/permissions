<?php

namespace SkillFactory\Permissions\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $casts = [
        'permissions' => 'array',
    ];

    protected $fillable = ['name', 'permissions'];

    public function getWhitelist()
    {
        return array_get($this->permissions, 'whitelist', []);
    }

    public function getBlacklist()
    {
        return array_get($this->permissions, 'blacklist', []);
    }
}
