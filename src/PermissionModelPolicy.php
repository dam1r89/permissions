<?php

namespace SkillFactory\Permissions;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Database\Eloquent\Model;

class PermissionModelPolicy
{
    use HandlesAuthorization;

    public function read(User $user, Model $model)
    {
        $requiredPermission = get_class($model).'.read';

        return $user->hasPermission($requiredPermission);
    }

    public function delete(User $user, Model $model)
    {
        $requiredPermission = get_class($model).'.delete';

        return $user->hasPermission($requiredPermission);
    }

    public function save(User $user, Model $model)
    {
        $requiredPermission = get_class($model).'.save';

        return $user->hasPermission($requiredPermission);
    }
}
